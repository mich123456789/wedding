# -*- coding: utf-8 -*-
{
    'name': 'Wedding',
    'version': '0.1',
    "category": 'Wedding',
    'complexity': "easy",
    'description': """
    creating event
    """,
    'author': 'OpenERP SA',
    'depends': ['website'],

    'data': [
        'templates/drop_zone.xml'
    ],
    'installable': True,
    'application' : True,
}
