# -*- coding: utf-8 -*-
from openerp import models, fields, api, exceptions
import datetime
import locale

class WeddingAttachment(models.Model):
    _inherit = 'ir.attachment'

    is_wedding = fields.Boolean('Photo du marriage')
