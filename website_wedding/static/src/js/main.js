
function return_image(id){
    image = "<img src='/website/image/ir.attachment/"+id+"/datas/300x300'>"
    one_image = "<div class='photo' style='height:310px;width:310px;display:inline-block;'><a href='/picture/"+id+"'>"+image+"</a></div>";
    $("#gallery").append(one_image);
}

function get_photo(page) {
    openerp.jsonRpc("/get_images/", 'call', {'id': page}).then(function (data) {
        if (data) {
            for(var i= 0; i < data['images'].length; i++){
                return_image(data['images'][i]);
             }
        }
    });
}

get_photo(parseInt(1));

$('a#pager').click(function (event){
    $("#gallery").empty();
    get_photo(parseInt(this.name));
});

