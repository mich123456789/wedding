# -*- coding: utf-8 -*-

import base64
from openerp import http

class Wedding(http.Controller):
    par = 10

    @http.route('/upload', type='http', auth='public', website=True)
    def upload(self, **kw):
        return http.request.render('website_wedding.index', {})


    @http.route('/upload-target', type='http', auth='public', website=True)
    def upload_pictures(self, **kw):
        datas = base64.b64encode(http.request.params['file'].read())
        http.request.env['ir.attachment'].create({
            'datas':datas,
            'name': 'new_image_test',
            'is_wedding': True})
        return "OK"

    @http.route('/gallery/', type='http', auth='public', website=True)
    def show_galery(self):
        images_all = http.request.env['ir.attachment'].search([('is_wedding', '=', True)])
        length = (len(images_all)/self.par)+1
        return http.request.render('website_wedding.images', {'pages': range(1, length+1)})

    @http.route('/picture/<int:id>/', type='http', auth='public', website=True)
    def show_picture(self, id):
        return http.request.render('website_wedding.picture', {'image_id': id})


    @http.route('/get_images/', type='json', auth='public', website=True)
    def show_image(self, id):
        images = http.request.env['ir.attachment'].search([('is_wedding', '=', True)], limit=self.par,
                                                          offset=(id*self.par)-self.par)
        test = [image.id for image in images]
        return {'images': test}

    @http.route('/get_image/', type='json', auth='public', website=True)
    def show_one_image(self, id):
        return {'id': id}

    @http.route('/download_image/<int:id>', type='http', auth='public', website=True)
    def download_one_image(self, id):
        filecontent = http.request.env['ir.attachment'].browse(id).datas

        return http.request.make_response(filecontent,
                 [('Content-Type', 'application/octet-stream')])